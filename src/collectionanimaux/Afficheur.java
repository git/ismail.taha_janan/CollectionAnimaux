/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package collectionanimaux;

/**
 *
 * @author istahajana
 */
public class Afficheur {
    public void afficheurMenu(){
        System.out.println("************ MENU ***********");
        System.out.println("1 - Ajouter un animal");
        System.out.println("2 - Lister les animeaux");
        System.out.println("3 - Quitter");
        System.out.println("*******************************");
    }
    
    public void afficheurMenuAnimal(){
        System.out.println("************ MENU ***********");
        System.out.println("1 - Oiseaux");
        System.out.println("2 - Mammifère");
        System.out.println("3 - Poisson");
        System.out.println("*******************************");
    }
}
