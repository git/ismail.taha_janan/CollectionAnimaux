/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package collectionanimaux;

import java.util.Scanner;

/**
 *
 * @author istahajana
 */
public class Lecteur {
    Scanner sc = new Scanner(System.in);
    
    public int lireMenu(){
        int menuNum = sc.nextInt();
        return menuNum;
    }
    
    public int lireTypeAnimal(){
        int typeNum = sc.nextInt();
        return typeNum;
    }
}
